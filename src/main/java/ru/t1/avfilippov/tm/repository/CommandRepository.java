package ru.t1.avfilippov.tm.repository;

import ru.t1.avfilippov.tm.api.ICommandRepository;
import ru.t1.avfilippov.tm.constant.ArgumentConst;
import ru.t1.avfilippov.tm.constant.CommandConst;
import ru.t1.avfilippov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command INFO = new Command(
            CommandConst.INFO, ArgumentConst.INFO,
            "show system info"
    );

    private static final Command ABOUT = new Command(
            CommandConst.ABOUT, ArgumentConst.ABOUT,
            "show developer's info"
    );

    private static final Command VERSION = new Command(
            CommandConst.VERSION, ArgumentConst.VERSION,
            "show application version"
    );

    private static final Command HELP = new Command(
            CommandConst.HELP, ArgumentConst.HELP,
            "show command list"
    );

    private static final Command EXIT = new Command(
            CommandConst.EXIT, null,
            "close application"
    );

    public static final Command[] COMMANDS = new Command[]{
            ABOUT, INFO, VERSION, HELP, EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}
