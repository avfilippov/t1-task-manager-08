package ru.t1.avfilippov.tm;

import ru.t1.avfilippov.tm.api.ICommandRepository;
import ru.t1.avfilippov.tm.constant.ArgumentConst;
import ru.t1.avfilippov.tm.constant.CommandConst;
import ru.t1.avfilippov.tm.model.Command;
import ru.t1.avfilippov.tm.repository.CommandRepository;
import ru.t1.avfilippov.tm.util.FormatUtil;

import java.util.Scanner;


public final class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        processArguments(args);
        processCommands();
    }

    public static void processCommands() {
        final Scanner scanner = new Scanner(System.in);
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static void processArguments(String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArgument(argument);
    }

    public static void processArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.INFO:
                showSystemInfo();
                break;
            default:
                showErrorArgument();
                break;
        }
    }

    public static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.EXIT:
                showExit();
                break;
            case CommandConst.INFO:
                showSystemInfo();
                break;
            default:
                showErrorCommand();
                break;
        }
    }

    private static void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final long availableProcessors = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final String freeMemoryFormat = FormatUtil.format(freeMemory);
        final long maxMemory = runtime.maxMemory();
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = (maxMemoryCheck ? "no limit" : FormatUtil.format(maxMemory));
        final long totalMemory = runtime.totalMemory();
        final String totalMemoryFormat = FormatUtil.format(totalMemory);
        final long usageMemory = totalMemory - freeMemory;
        final String usageMemoryFormat = FormatUtil.format(usageMemory);

        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Maximum memory: " + maxMemoryFormat);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Usage memory: " + usageMemoryFormat);
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Alexey Filippov");
        System.out.println("Email: avfilippov@t1-consulting.ru");
    }

    private static void showErrorArgument() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported");
        System.exit(1);
    }

    private static void showErrorCommand() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.8.0");
    }

    private static void showExit() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REPOSITORY.getCommands();
        for (final Command command : commands) System.out.println(command);
    }

}
